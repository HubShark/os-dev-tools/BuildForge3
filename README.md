# BuildForge3

BuildForge3 is sort of a continuation of BuildForge2. Only this time I'm starting with a script that I have NOT tested.

This script is also at the gnewsense bzr repo. I want to start with the script as I scanned over it, I felt it could solve some problems I've had with BuildForge2.

Once I have a basic system building and working correctly, I'll be adding portions of BF2 back into this set of scripts.


**BuildForge** is a set of scripts originally designed by Brian Brazil, named _builder_ and used by the gNewSense project.
The gNewSense project removed "_non-free_" things from many parts of _Ubuntu_, which serverd as a base for thier Operating System. The gNewSense project has abandoned the _builder scripts_ in favour of thier new _debderiver_.

BuildForge is based on the "builder" rev.430 and reworked, modded, hacked - to fit the needs of the
Cybernux Project. Debian is the _Upstream Base distribution_. Not only are we not removing the stuff that gNewSense did, but we may add more to it.

We will remove things from the repository that we don't feel "needs to be there". This may be Debian Specific stuff or some of the CMSs, various web applications ... anything that's easily gotten somewhere online. An example of this is Drupal or Archipel - in the repo the will be old very quickly, so they're just using space. So for us it's considered **_"Not-4-Us"_**.



License(s)
==========

LICENSE DETAILS:	see COPYING

GNEWSENSE BUILDER DETAILS:	see README



ISSUES, BUGS & Co.
==================

Issues, bugs, sugesstion, etc. can be (should be) places in our [ISSUES](../../issues).

We will also keep a _MILESTONE_ setup so you can keep updated as to where devlopment is at.



Documentation
=============

As far as possible we will keep an updated [WIKI](../../wikis/home) on the BuildForge scripts. There may be some documentation in the _docs_ folder, but they may be somewhat old.

Getting started information is also in the [WIKI](../../wikis/StartHere)



----

	-- hyperclock, Apr 2019
